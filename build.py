#   -*- coding: utf-8 -*-
from pybuilder.core import use_plugin, init, Author

use_plugin("python.core")
use_plugin("python.unittest")
use_plugin("python.flake8")
use_plugin("python.coverage")
use_plugin("python.distutils")
use_plugin('python.pycharm')


name = "topologyrefactor"
authors = [Author("Adam Ridouane", ''),]
description = ""
version = '1.0.0'
default_task = "publish"

@init
def set_properties(project):
    project.depends_on_requirements("requirements.txt")
    project.build_depends_on("mockito")
    project.set_property("coverage_exceptions", ["topologyrefactor.utils", "topologyrefactor.runner"])
    project.set_property('distutils_entry_points', {
        'console_scripts': [
            "topologyrefactor = topologyrefactor.runner:run",
        ]})
