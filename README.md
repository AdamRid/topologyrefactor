Pour installer topologyrefactor:

- Option 1:<br />
À travers de pip:<br />
    `pip install https://gitlab.com/AdamRid/topologyrefactor/-/raw/master/dist/topologyrefactor-1.0.0.tar.gz`
    
- Option 2:<br />
Télécharger le projet et installer localement:<br />
    `git clone https://gitlab.com/AdamRid/topologyrefactor.git`<br />
    `cd topologyrefactor`<br />
    `python setup.py install`<br />
    
    
Usage:<br />
`topologyrefactor fichierA.xml`<br />
    -> Ça va créer un fichierA.json dans le directoire de travail actuel

`topologyrefactor fichierA.xml fichierB.xml ... fichierN.xml`<br />
    -> Ça va créer un fichier 'mixed.json' contenant tous les CCObject qui 
       apparaissent dans les différents ffichierx.xml