# -*- coding: utf-8 -*-
import os
import sys
import json
from collections import OrderedDict

import xmltodict

from model import CCObject, CCScalarAttribute, CCVectorAttribute, CCTableAttribute
from utils import error_catcher

CC_OBJECT_LIST = 'CCObjectList'
CC_OBJECT = 'CCObject'


class File:
    def __init__(self, file_path):
        self.file_path = file_path

    @error_catcher
    def read_xml(self):
        with self.file_path and open(self.file_path) as fr:
            return xmltodict.parse(fr.read())


class TopologyFile(File):
    def __init__(self, file_path=None):
        File.__init__(self, file_path)
        self.raw_data = self.read_xml() if file_path else None
        self.refactored_data = self.refactor_xml_file() if file_path else None

    def get_refactored_data(self):
        return self.refactored_data

    def set_refactored_data(self, parsed_data):
        self.refactored_data = parsed_data

    @error_catcher
    def refactor_xml_file(self):
        refactored_data = OrderedDict()
        refactored_data[CC_OBJECT] = list()
        xml_cc_objects = self.raw_data[CC_OBJECT_LIST][CC_OBJECT] \
            if CC_OBJECT_LIST in self.raw_data and CC_OBJECT in self.raw_data[CC_OBJECT_LIST] else list()
        json_cc_objects = list()
        if type(xml_cc_objects) is OrderedDict:
            json_cc_object = self._refactor_xml_cc_object(xml_cc_objects)
            json_cc_objects.append(json_cc_object.get_refactored_cc_object())
        elif type(xml_cc_objects) is list:
            for xml_cc_object in xml_cc_objects:
                json_cc_object = self._refactor_xml_cc_object(xml_cc_object)
                json_cc_objects.append(json_cc_object.get_refactored_cc_object())
        return json_cc_objects

    @classmethod
    def _refactor_xml_cc_object(cls, xml_cc_object):
        cc_object = CCObject(xml_cc_object["@businessId"], xml_cc_object["@type"])
        if type(xml_cc_object["CCAttribute"]) is list:
            for attribute in xml_cc_object["CCAttribute"]:
                cc_attribute = cls._refactor_xml_cc_attribute(attribute)
                cc_object.add_cc_attribute(cc_attribute)
        else:
            cc_attribute = cls._refactor_xml_cc_attribute(xml_cc_object["CCAttribute"])
            cc_object.add_cc_attribute(cc_attribute)
        return cc_object

    @classmethod
    def _refactor_xml_cc_attribute(cls, xml_cc_attribute):
        if "CCScalar" in xml_cc_attribute:
            return CCScalarAttribute(xml_cc_attribute["@businessId"], xml_cc_attribute)
        elif "CCVector" in xml_cc_attribute:
            return CCVectorAttribute(xml_cc_attribute["@businessId"], xml_cc_attribute)
        elif "CCTable" in xml_cc_attribute:
            return CCTableAttribute(xml_cc_attribute["@businessId"], xml_cc_attribute)

    @error_catcher
    def write_json(self, output_file_name=None):
        output_data = OrderedDict()
        output_data["CCObject"] = self.refactored_data if self.refactored_data else list()
        output_file_name = output_file_name if output_file_name else str(self.file_path).replace("xml", "json")
        with open(output_file_name, "w") as fw:
            data = json.dumps(output_data, indent=4)
            fw.write(data)
            return output_file_name


def run():
    if len(sys.argv) <= 1:
        sys.stdout.write('\nErreur: Aucun fichier XML indiquee')
        exit(1)
    topology_files = sys.argv[1:]
    for topology_file in topology_files:
        if not os.path.isfile(topology_file):
            sys.stdout.write("\nErreur: le fichier '%s' n'a pas ete trouve\n" % topology_file)
            exit(2)

    if len(topology_files) == 1:
        topology = TopologyFile(file_path=topology_files[0])
        output_file = topology.write_json()
    else:
        parsed_data = list()
        for topology_file in topology_files:
            topology = TopologyFile(file_path=topology_file)
            parsed_data += topology.get_refactored_data()

        topology = TopologyFile()
        topology.set_refactored_data(parsed_data)
        output_file = topology.write_json("mixed.json")
    sys.stdout.write("\nLe fichier '%s' a etait cree\n" % output_file)
