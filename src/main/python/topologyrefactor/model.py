from collections import OrderedDict


class CCObject:
    def __init__(self, business_id, type):
        self.business_id = business_id
        self.type = type
        self.cc_attributes = list()

    def add_cc_attribute(self, cc_attribute):
        if cc_attribute:
            self.cc_attributes.append(cc_attribute)

    def add_cc_vector_attribute(self):
        pass

    def get_refactored_cc_object(self):
        cc_object = OrderedDict()
        cc_object["key"] = self.business_id
        cc_object["val"] = OrderedDict()
        cc_object["val"]["attr"] = list()
        cc_object["val"]["typeId"] = self.type
        for cc_attribute in self.cc_attributes:
            cc_object["val"]["attr"].append(cc_attribute.get_refactored_cc_attribute())
        return cc_object


class CCAttribute:
    def __init__(self, business_id, raw_value):
        self.business_id = business_id
        self.raw_value = raw_value
        self.value = None

    def _refactor_raw_data(self):
        pass

    def get_refactored_cc_attribute(self):
        pass

    def _get_dict(self):
        cc_attribute = OrderedDict()
        cc_attribute["bid"] = self.business_id
        cc_attribute["val"] = self.value
        cc_attribute["known"] = True
        return cc_attribute


class CCScalarAttribute(CCAttribute):
    def __init__(self, business_id, raw_value):
        CCAttribute.__init__(self, business_id, raw_value)

    def _refactor_raw_data(self):
        self.value = self.raw_value["CCScalar"]["@value"] \
            if self.raw_value["CCScalar"] and "@value" in self.raw_value["CCScalar"] else str()

    def get_refactored_cc_attribute(self):
        self._refactor_raw_data()
        return self._get_dict()


class CCVectorAttribute(CCAttribute):
    def __init__(self, business_id, raw_value):
        CCAttribute.__init__(self, business_id, raw_value)

    def _refactor_raw_data(self):
        self.value = self.raw_value["CCVector"]["CCScalar"] \
            if self.raw_value["CCVector"] and "CCScalar" in self.raw_value["CCVector"] else list()
        if type(self.value) is list and self.value:
            self.value = [item["@value"] for item in self.value]
        elif type(self.value) is OrderedDict and self.value:
            self.value = [self.value["@value"]]

    def get_refactored_cc_attribute(self):
        self._refactor_raw_data()
        return self._get_dict()


class CCTableAttribute(CCAttribute):
    def __init__(self, business_id, raw_value):
        CCAttribute.__init__(self, business_id, raw_value)

    def _refactor_raw_data(self):
        self.value = self.raw_value["CCTable"]["CCTableLine"] \
            if self.raw_value["CCTable"] and "CCTableLine" in self.raw_value["CCTable"] else list()
        if type(self.value) is list and self.value:
            self.value = [[inner_item["@value"] for inner_item in item["CCScalar"]] for item in self.value]
        elif type(self.value) is OrderedDict and self.value:
            self.value = [[item["@value"] for item in self.value["CCScalar"]]]

    def get_refactored_cc_attribute(self):
        self._refactor_raw_data()
        return self._get_dict()