# -*- coding: utf-8 -*-
import sys
import traceback
from functools import wraps

import runner


def error_catcher(f):
    @wraps(f)
    def catcher(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception as e_:
            if f.__name__ == runner.File.read_xml.__name__:
                sys.stdout.write("\nErreur: Fichier XML avec format incorrect\n")
            elif f.__name__ == runner.TopologyFile.refactor_xml_file.__name__:
                sys.stdout.write("\nErreur: le fichier XML n'est peut-etre pas un fichier de la topologie CC\n")
            elif f.__name__ == runner.TopologyFile.write_json.__name__:
                sys.stdout.write("\nErreur: Le fichier json n'a pas pu etre cree\n")
            else:
                sys.stdout.write("\nErreur inconnu. Fonction origine: %s\n" % f.__name__)
            sys.stdout.write(str(traceback.format_exception_only(type(e_), e_)))
            sys.stdout.write("\n" + traceback.format_exc())
            exit(1)
    return catcher
