import os
import unittest

from topologyrefactor.runner import TopologyFile


ROOT_PATH = os.path.dirname(os.path.abspath(__file__))


class RunnerTest(unittest.TestCase):
    def test_read_xml_file_01(self):
        file_path = os.path.join(ROOT_PATH, "..", "sources", "quai.xml")
        topology = TopologyFile(file_path=file_path)
        data = topology.get_refactored_data()
        self.assertEqual(len(data), 79, "le resultat json pour le fichier quai.xml devrait contenir 79 quais")
        topology.write_json()

    def test_read_xml_file_02(self):
        file_path = os.path.join(ROOT_PATH, "..", "sources", "listes.xml")
        topology = TopologyFile(file_path=file_path)
        data = topology.get_refactored_data()
        self.assertEqual(len(data), 1, "le resultat json pour le fichier listes.xml devrait contenir 1 listes")
        topology.write_json()

    def test_read_xml_file_03(self):
        file_path = os.path.join(ROOT_PATH, "..", "sources", "moniteur.xml")
        topology = TopologyFile(file_path=file_path)
        data = topology.get_refactored_data()
        self.assertEqual(len(data), 0, "le resultat json pour le fichier moniteur.xml devrait contenir 0 moniteur")
        topology.write_json()

    def test_read_xml_file_04(self):
        file_path = os.path.join(ROOT_PATH, "..", "sources", "marcheType.xml")
        topology = TopologyFile(file_path=file_path)
        data = topology.get_refactored_data()
        self.assertEqual(len(data), 4, "le resultat json pour le fichier marcheType.xml devrait contenir 0 marcheType")
        topology.write_json()